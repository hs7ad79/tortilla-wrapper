/**
 * @file    uart_example.cpp
 * @author Hugo Schaaf (https://github.com/hugoschaaf98)
 * @brief   Example using tortilla Uart class
 * 		   	compile with <g++ -Wall -Wextra -pedantic -std=c++17 -I../ -o bin/uart_example uart_example.cpp>
 * @version 0.1
 * @date 2021-10-29
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <array>
#include <cstdint>
#include <iostream>

#include "tortilla.h"

std::uint8_t uartReadChar()
{
    return std::cin.get();
}

void uartWriteChar(std::uint8_t c)
{
    std::cout << c;
}

template <std::size_t N_>
using u8Array = std::array<std::uint8_t, N_>;

int main()
{
    u8Array<100> ioBuffer{0};
    // Create an I2C object
    tia::Uart myUart{uartReadChar, uartWriteChar};

    // Test tia::Uart::readChar() with standart input
    std::cout << "\nTest tia::Uart::readChar() with standart input implementation" << std::endl;
    std::cout << "Please type some character in and hit <Enter> key." << std::endl;

    for (auto &e : ioBuffer)
    {
        auto c = myUart.readChar();
        if (c == '\n')
            break;
        e = c;
    }

    // Test tia::Uart::writeChar() with standart output
    std::cout << "\nTest tia::Uart::writeChar() with standart output implementation" << std::endl;
    std::cout << "Received <";
    for (auto &e : ioBuffer)
    {
        myUart.writeChar(e);
    }
    std::cout << ">" << std::endl;

    return 0;
}